require 'sidekiq/web'
Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount Sidekiq::Web => '/sidekiq'
  mount ActionCable.server => "/cable"
  root to: "article#index"
  get '/admin', to: "article#new"
  post '/admin',to: "article#create"
end
