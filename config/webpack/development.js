const environment = require('./environment')
environment.loaders.set('json', { 
  test: /\.less$/, 
  loader: 'style-loader!css-loader!less-loader'
});
module.exports = environment.toWebpackConfig()