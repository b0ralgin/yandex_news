class ArticleController < ActionController::Base
  def index
    @article = Article.last_active || Article.new
    respond_to do |format|
      format.json do
        render json: {
          title: @article.title,
          abstract: @article.abstract,
          publish_date: @article.publish_date
      }
      end
      format.html do
        @article
      end
    end
  end

  def new; end

  def create
    @article = Article.new article_params
    if @article.save
      redirect_to action: 'index'
    else
      render json: @article.errors, status: :bad_request
    end
  end

  private

  def time_to_int(time)
    hours = time.split(':')[0].to_i
    minutes = time.split(':')[1].to_i
    return hours.hour + minutes.minute
  end

  def article_params
    params.require(:article)
          .permit(:title, :abstract, :publish_date, :expire_in)
          .merge(expire_in: Time.now + time_to_int(params[:article][:expire_in]))
  end
end

