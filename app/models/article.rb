class Article < ApplicationRecord
  before_save :change_time, :broadcast_article
  after_save :queue_fetch
  validates :title, :abstract,:publish_date, presence: true
  validates :title, length: { maximum: 140 }
  validates :publish_date, date: { before_or_equal_to: proc { Time.now}, message: 'cannot be in future' }
  validates :expire_in, date: { before: proc { Time.now + 24.hours}, message: 'cannot be more than 24 hours' }

  scope :last_active, -> { order(publish_at: :desc).first }

  def update_article(article_atributes)
    self.title = CGI.unescapeHTML(article_atributes.title)
    self.abstract = CGI.unescapeHTML(article_atributes.abstract)
    self.publish_date = Time.zone.parse(article_atributes.to_s).iso8601
    self.expire_in = article_atributes.expire_in
    self.save!
  end

  def self.save_article(yandex_article)
    article = self.last_active || self.new
    return if article[:expire_in] > yandex_article[:expire_in]
    if article.update_article(yandex_article)
      logger.error "Error in article #{article.errors.full_messages}" 
    else 
      logger.info "Article is saved #{article.id}" 
    end
  end
  
  private

  def change_time
    self.publish_at ||= Time.now
  end

  def broadcast_article
    article = {title: title, abstract: abstract, publish_date: publish_date }
    BroadcastArticleJob.perform_async(article)
  end

  def queue_fetch
    FetchNewsJob.perform_at(expire_in + 1.second)
  end

end
