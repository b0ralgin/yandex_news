
# Class to download and parse news from Yandex RSS
class FetchNewsJob 
  include Sidekiq::Worker
  sidekiq_options :retry => false
  RSS_URL = 'https://news.yandex.ru/index.rss'
  YandexArticle = Struct.new(:title, :abstract, :publish_date, :publish_at, :expire_in)
  def perform
    require 'rss'
    require 'open-uri'
    expiration_time = Time.now + 1.minute
    rss = RSS::Parser.parse(open(RSS_URL).read, false)
                     .items
                     .first
    yandex_article = YandexArticle.new(
       rss.title,
       rss.description,
       rss.pubDate,
       Time.now,
       expiration_time,
    )
    logger.info("Get article from RSS #{yandex_article}")
    Article.save_article yandex_article
    
      # TODO add an errors handling
  end
end


