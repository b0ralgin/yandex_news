class BroadcastArticleJob
  include Sidekiq::Worker
  sidekiq_options retry: 20.seconds
  def perform(article)
    article.symbolize_keys!
    ActionCable.server.broadcast(
      'article',
      title: article[:title],
      abstract: article[:abstract],
      publish_date: article[:publish_date]
    )
  end
end


