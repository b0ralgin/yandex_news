
//var moment = require('moment');
import moment from 'moment';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ReactDateTime from 'react-datetime';
import TimeInput from 'react-time-input';

class ArticleForm extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      abstract: "",
      publishDate: moment(),
      expireAt: '00:01', //TODO добавить 1 минуту
      errors: []
    };
    this.sendArticle = this.sendArticle.bind(this)
  }

  sendArticle(title, abstract,publishDate,expireAt) {
    var self = this 
    fetch('/admin', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        title: title, 
        abstract: abstract, 
        publish_date: publishDate, 
        expire_in: expireAt 
      }),
    }).then(  
      response => {
        if (response.status !== 200) {  
          if (response.status == 400) {
            response.json().then( data => {
              console.log(data)
              self.setState({errors: data})
            })
            return 
          }
        } else {
          window.location = response.url
        }
      }
    )
    .catch(function(err) {  
      console.log('Fetch Error :-S', err);  
    });

  }  
  
  handleTitle = e => {
    this.setState({ title: e.target.value });
  };
  handleAbstract = e => {
    this.setState({abstract: e.target.value})
  };

  handleDateChange = m => {
    console.log()
    this.setState({ publishDate: m });
  };
  handeTimeChange = m => {
    this.setState({expireAt: m})
  }

  handleSave = () => {
    console.log('saved', this.state.m.format('llll'));
  };
  handleSubmit = (e) => {
    e.preventDefault();
    var title = this.state.title.trim();
    var abstract = this.state.abstract.trim();
    var publishDate = this.state.publishDate;
    var expireAt = this.state.expireAt.trim();
    //if !this.state.valid() return 
    this.sendArticle(title, abstract,publishDate,expireAt)
  }
  
  render() {
    
    return (
      <form className='container' onSubmit={this.handleSubmit}>
        <div className='form-group'>
        <h2 className="form-signin-heading">Title</h2>
          <label className="Error" >
            {this.state.errors['title']}
          </label>
          <input type='text' className='title'
                 placeholder='Title' name='title'
                onChange={this.handleTitle}>
          </input>
        <h2 className="form-signin-heading">Abstract</h2>
        <label className="Error" >
            {this.state.errors['abstract']}
          </label>
          <input type='text' className='abstract'
                 placeholder='Abstract' name='abstract'
                 onChange={this.handleAbstract}>
          </input>
        <h2 className="form-signin-heading">Publication Date</h2>
        <label className="Error" >
            {this.state.errors['publish_date']}
          </label>
          <div className="input">
            <ReactDateTime
            input={false}
            defaultValue={this.state.publishDate}
            onChange={this.handleDataChange}
            />
          </div>
        <h2 className="form-signin-heading">Expiration Date</h2>
          <label className="error" >
              {this.state.errors['expire_in']}
          </label>
          <div className="input">
            <TimeInput
            initTime={this.state.expireAt}
            ref="TimeInputWrapper"
            className='form-control'
            onTimeChange={this.handeTimeChange}
            />
          </div>
        <input className="btn btn-large btn-primary" type="submit" value="Post"/>
        </div>
      </form>
      );
    }
  }
  document.addEventListener('DOMContentLoaded', () => {
    ReactDOM.render(
      <ArticleForm />,
      document.body.appendChild(document.createElement('div')),
    )
  })