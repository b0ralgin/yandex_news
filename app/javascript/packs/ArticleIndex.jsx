import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ActionCableProvider  from 'react-actioncable-provider'
import moment from 'moment';
class ArticleIndex extends React.Component {
 
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      abstract: "",
      publish_date: ""
    };
    this.getArticle = this.getArticle.bind(this);
    this.updateArticle = this.updateArticle.bind(this);
  }
  updateArticle(article) {
    console.log(article)
    this.setState({
      title: article.title  ,
      abstract: article.abstract,
      publish_date: article.publish_date ,
    });
  }
  getArticle(){
    fetch('/', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
    .then(status)
    .then(json)
    .then(responseJson => {
      this.setState( {
        title: responseJson.title  ,
        abstract: responseJson.abstract,
        publish_date: responseJson.publish_date ,
      });
       
    }) 
  }
  setupSubscription(){
    
      App.comments = App.cable.subscriptions.create("ArticleChannel", {

        connected: function () {},
    
        received: function (data) {
          this.updateArticle(data);
        },
    
        updateArticle: this.updateArticle   
        });
    }
    componentWillMount() {
       this.getArticle();
    }
    componentDidMount() {
      this.setupSubscription();
    }
  render () {
    console.log(this.state.pubDate)
    return (
        <div className="container">
          <h1 className="title ">{this.state.title}</h1>
          <h4 className="date ">{moment(this.state.publish_date).locale('ru').format('dddd, MMMM DD YYYY, hh:mm')}</h4>
          <p className="abstract lead">{this.state.abstract}</p>
          <a className="btn btn-large" href="/admin">
            Add own article
          </a>
        </div>
    );
  }
}
function status(response) {  
  if (response.status >= 200 && response.status < 300) {  
    return Promise.resolve(response)  
  } else {  
    return Promise.reject(new Error(response.statusText))  
  }  
}
function json(response) {  
  return response.json()  
}
function getData(data) {
}

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <ArticleIndex/>,
    document.body.appendChild(document.createElement('div')),
  )
})
