import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ActionCableProvider  from 'react-actioncable-provider'
class ArticleIndex extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.title,
      abstract: this.props.abstract,
      pubDate: this.props.pubDate,
    }
    this.updateArticle = this.updateArticle.bind(this);
  }
  updateArticle(article) {
    console.log(article)
    this.setState({title: article.title,
    abstract: article.abstract,
    pubDate: article.publish_date});
  }
  setupSubscription(){
    
      App.comments = App.cable.subscriptions.create("ArticleChannel", {

        connected: function () {},
    
        received: function (data) {
          this.updateArticle(data);
        },
    
        updateArticle: this.updateArticle   
        });
    }
    componentDidMount() {
      this.setupSubscription();
    }
  render () {
    return (
        <div className="container">
          <h1 className="title ">{this.state.title}</h1>
          <h4 className="date ">{this.state.pubDate}</h4>
          <p className="abstract lead">{this.state.abstract}</p>
          <a className="btn btn-large" href="/admin">
            Edit article
          </a>
        </div>
    );
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('article')
  const data = JSON.parse(node.getAttribute('data'))
  ReactDOM.render(
    <ArticleIndex {...data}/>,
    document.body.appendChild(document.createElement('div')),
  )
})
