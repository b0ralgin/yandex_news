

shared_examples "an unauthorized access" do
  it "should return http status 401" do
    expect(response.response_code).to eq 401
  end
end

shared_examples "a forbidden access" do
  it "should return http status 403" do
    expect(response.response_code).to eq 403
  end
end

shared_examples "an ok html response" do
  it "should retrieve a content-type of html with response code of 200" do
    expect(response.header["Content-Type"]).to include "text/html"
    expect(response.response_code).to eq 200
  end
end

shared_examples "an ok json response" do
  it "should retrieve a content-type of json with response code of 200", return_format: true do
    expect(response.header["Content-Type"]).to include "application/json"
    expect(response.response_code).to eq 200
  end
end

shared_examples "an ok pdf response" do
  it "should retrieve a content-type of pdf with response code of 200", return_format: true do
    expect(response.header["Content-Type"]).to include "application/pdf"
    expect(response.response_code).to eq 200
  end
end

shared_examples "an ok destroy response" do
  it "should return http status 204", return_format: true do
    expect(response.response_code).to eq 204
  end
end

shared_examples "an ok xlsx response" do
  it "should retrieve a content-type of xlsx with response code of 200" do
    expect(response.response_code).to eq 200
    expect(response.header["Content-Type"]).to include "spreadsheet"
  end
end

shared_examples "a valid create json response" do
  it "should retrieve a content-type of json with response code of 201" do
    expect(response.header["Content-Type"]).to include "application/json"
    expect(response.response_code).to eq 201
  end
end

shared_examples "a not found" do
  it "should retrieve a content-type of json with response code of 404" do
    expect(response.response_code).to eq 404
  end
end

shared_examples "an error json response" do
  it "should retrieve a content-type of json with response code of 412" do
    expect(response.header["Content-Type"]).to include "application/json"
    expect(response.response_code).to eq 412
  end
end

shared_examples "unavailable for legal reasons" do
  it "should retrieve a content-type of json with response code of 451" do
    expect(response.header["Content-Type"]).to include "application/json"
    expect(response.response_code).to eq 451
  end
end

shared_examples "a not implemented" do
  it "should retrieve a content-type of jsonwith response code of 501" do
    expect(response.header["Content-Type"]).to include "application/json"
    expect(response.response_code).to eq 501
  end
end

shared_examples "a redirect to index" do
  it "should retrieve a location with index and with response code of 302" do
    response.should redirect_to(article_path(assigns[:index]))
    expect(response.response_code).to eq 302
  end
end

shared_examples "a valid S3 token" do
  it "should contain mandatory info" do
    json = JSON.parse response.body
    %w[policy signature access_key bucket path].each do |k|
      expect(json[k]).to_not be_empty, "`#{k}` should not be empty"
    end

    expect(json["path"]).to_not be_empty
  end
end

shared_examples "an internal error" do
  it "should return http status 500" do
    expect(response.response_code).to eq 500
  end
end

shared_examples "an empty response" do
  it_behaves_like "an ok json response"
  it "should return an empty collection" do
    json = JSON.parse response.body
    expect(json).to be_empty
  end
end