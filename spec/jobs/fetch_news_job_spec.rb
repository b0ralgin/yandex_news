require "spec_helper"
require 'factory_bot_rails'
require 'sidekiq/testing'

RSpec.configure do |config| 
  config.before(:each) do
    Sidekiq::Worker.clear_all
  end
end

RSpec.describe FetchNewsJob, :type => :job do
  Sidekiq::Testing.fake! do 
    describe "#save_article" do
      context "when article in new" do
        let(:article) { FactoryBot.create :article, expire_in: Time.now - 1.second } 
        let(:expiration_time) {Time.now + 1.minute}
        YandexArticle = Struct.new(:title, :abstract, :publish_date, :publish_at, :expire_in)
        it "should add new job to queue" do
          expect do 
            yandex_article = YandexArticle.new(
              article.title,
              article.abstract,
              article.publish_date,
              Time.now,
              expiration_time
            )
            FetchNewsJob.perform_one
          end.to change(FetchNewsJob.jobs, :size).by(1)
        end
      end
    end
  end
end




