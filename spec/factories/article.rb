
FactoryBot.define do
  factory :article  do
    title "Title"
    abstract "Abstract"
    sequence(:publish_date) {|n|  Time.now  }
    sequence(:expire_in) { |n| Time.now + n.minutes }
  end
end
