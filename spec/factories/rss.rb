FactoryBot.define do
  factory :rss  do
    sequence(:title) { |n| "Title #{n}" }
    sequence(:publish_date) {  Time.now  }
    abstract "Abstract"
  end
end