require "spec_helper"
require "shared_examples"
require 'factory_bot_rails'
require 'rails_helper'

RSpec.describe ArticleController, :type => :request do
  let(:title) { "Title" }
  let(:abstract) { "Abstract"}
  let(:no_title) { "" }
  let(:no_abstract) { "" }
  describe ".index" do
    let(:article) {FactoryBot.create :article}
    let(:title) { "Title" }
    let(:abstract) { "Abstract"}
    let(:json) { JSON.parse(response.body) }
    context "when request in html" do
      before(:each) do
        get "/", as: :html
      end
      it_behaves_like "an ok html response"
    end
    context "when requset in json" do
      before(:each) do
        @article = FactoryBot.create( :article, title: title, abstract: abstract)
        get "/", as: :json
      end
      it_behaves_like "an ok json response"
      it "should include a title" do
        expect(json['title']).to eq "Title"
      end
    end
  end

  describe ".create" do 
    context "when params is valid" do
      let(:publish_date) { 1.hour.ago.to_s }
      let(:expire_in) { "00:01" }
      let(:params) { {
        title: title,
        abstract: abstract,
        publish_date: publish_date,
        expire_in: expire_in,
      } }
      it "should add artice" do 
        expect do 
          post "/admin", params: {article: params }
        end.to change { Article.count }.by(1)
      end
      it "should redirect" do 
        post "/admin", params: {article: params }
        expect(response.response_code).to eq 302
        expect(response).to redirect_to action: :index
      end
    end


    context "when params is invalid" do
      let(:wrong_publish_date) { 1.hour.after.to_s }
      let(:wrong_expire_in) { "00:00" }
      let(:wrong_params) { {
        title: no_title,
        abstract: no_abstract,
        publish_date: wrong_publish_date,
        expire_in: wrong_expire_in,
      } }
      it "should not add artice" do 
        expect do 
          post "/admin", params: {article: wrong_params }
        end.to change { Article.count }.by(0)
      end
      it "should send errors" do 
        post "/admin", params: {article: wrong_params }
        errors = JSON.parse(response.body).keys
        expect(errors).to include 'title'
        expect(errors).to include 'abstract'
        expect(errors).to include 'publish_date'
      end
      end
  end
end
