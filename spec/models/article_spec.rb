RSpec.describe ArticleController, :type => :model do
  let(:title) {"Title"}
  let(:abstract) {"Abstract"}
  let(:publish_date) { Time.now }
  let(:expire) { Time.now + 1.minute }
  describe "should validate atributes" do
    let(:params)  { {
      title: title,
      abstract: abstract,
      publish_date: publish_date,
      expire_in: expire,
    } }
    context "when all atributes are valid" do

      it "should save" do
        article = Article.new params
        expect(article.save).to be true
      end
    end
    context "when atribute is invalid" do
      before(:each) do
        params[:title] = ""
        @article = Article.new params
        @article.save
      end
      it "should not save" do
        expect(@article.id).to be_falsey
      end
      it "errors should include title" do
        expect(@article.errors.full_messages).to  include "Title can't be blank"
      end
    end
  end
end