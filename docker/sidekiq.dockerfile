FROM ruby:2.4.1
MAINTAINER kirill.borodylin@gmail.com
# Install essentials


# Packages pre-install requirements
RUN apt update && apt install -y  nodejs


RUN mkdir /app
WORKDIR /app

COPY Gemfile Gemfile.lock ./

RUN bundle install --binstubs

CMD ["bundle", "exec", "sidekiq" ]

