FROM ruby:2.4.1
MAINTAINER kirill.borodylin@gmail.com
# Install essentials


RUN apt-get update && apt-get install -y wget curl


# Packages pre-install requirements
RUN apt update && apt install -y nodejs

RUN mkdir /app
WORKDIR /app

COPY Gemfile Gemfile.lock ./

RUN bundle install --binstubs


CMD bundle exec rails s -b 0.0.0.0

EXPOSE 3000