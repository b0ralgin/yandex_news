class ChangeExpireAttoIn < ActiveRecord::Migration[5.0]
  def change
    rename_column :articles, :expire_at, :expire_in
  end
end
