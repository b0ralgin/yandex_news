class AddArticle < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :title
      t.string :abstract
      t.datetime :expire_at
    end
  end
end
