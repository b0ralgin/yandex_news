class AddPublishDateInArticle < ActiveRecord::Migration[5.0]
  def change
    add_column :articles, :publish_at, :datetime
    add_column :articles, :publish_date, :datetime
  end
end
